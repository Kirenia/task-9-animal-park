﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_9
{

   
    class Program
    {    

        static void Main(string[] args)
        {
            Animal Dog = new Animal("dog", "Pluto", 5, 4,new string[]{"animal"});
            Animal Cat = new Animal("cat", "Fun", 2, 3, new string[] { "animal" });
            Animal Bird = new Animal("Bird","Plint",3,2, new string[] { "plants" });
            Animal Human = new Animal("Human", "Maria", 67,2, new string[] { "animal","Plants" });

            List<Animal> MyList = new List<Animal>();
            MyList.Add(Dog);
            MyList.Add(Cat);
            MyList.Add(Bird);
            MyList.Add(Human);
           

            foreach (var item in MyList)
            {
                Console.WriteLine($"{item.name} was born in {item.YearOfbird(2018)} and is: "); item.AnimalKind();
            }
            
           
            Console.ReadLine();
        }
    }
}
