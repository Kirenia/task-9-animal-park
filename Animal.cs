﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_9
{
    public class Animal
    {
       
        public string name { get; set; }
        public string specie { get; set; }
        public int legs { get; set; }
        public int age { get; set; }
        public int wing { get; set; }
        public string[] food { get; set; }

        public Animal(string Aspecie, string Aname, int Aage, int Alegs,string[] Food)
        {
            specie = Aspecie;
            name = Aname;
            age = Aage;
            legs = Alegs;
            food = Food;
        }
        public Animal(string Aspecie, string Aname, int Aage, int Awing)
        {
            specie = Aspecie;
            name = Aname;
            age = Aage;
            wing = Awing;
            
        }
        


        public int YearOfbird(int year)
        {
            return year - age;
        }

        public void AnimalKind()
        {
            if (food.Contains("animal") && !food.Contains("plants"))
            {
                Console.WriteLine("Carnivores");
            }
            else if (!food.Contains("animal") && food.Contains("plants"))
            {
                Console.WriteLine("Herbivores");
            }
              else
            {
                Console.WriteLine("Omnivores");
            }       
               
           
        }

    }
}
